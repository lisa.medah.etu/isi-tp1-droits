#!/bin/bash

echo "L'accès aux dossiers de dir_a, dir_b et dir_c :
"
echo "Lecture d'un fichier dans dir_b:"
sudo -u lambda_b bash -c 'cat dir_b/admins_file.txt' 

echo "Edition d'un fichier dans dir_b:"
sudo -u lambda_b bash -c 'echo "lambda_b is writing" >> dir_b/admins_file.txt' 
sudo -u lambda_b bash -c 'cat dir_b/admins_file.txt' 

echo "Creation d'un fichier dans dir_b:"
sudo -u lambda_b bash -c 'touch dir_b/lambda_b_file.txt && echo "Le fichier a été créé avec succés dans dir_b par lambda_b"'

echo "Renommage du fichier qui a été créé par lambda_b dans dir_b:
Nom actuel:"
sudo -u lambda_b bash -c ' ls dir_b/ | grep "lambda_b"'
sudo -u lambda_b bash -c 'mv dir_b/lambda_b_file.txt dir_b/lambda_b_file_renamed.txt' 

echo "Nouveau nom/:"
sudo -u lambda_b bash -c ' ls dir_b/ | grep "lambda_b"'

echo "Tentative de suppression d'un fichier d' admin par lambda_b dans dir_b:"
sudo -u lambda_b bash -c 'rm -f dir_b/admins_file.txt'

echo " Tentative de rennomage d'un fichier d'admin par lambda_b dans dir_b:"
sudo -u lambda_b bash -c 'mv dir_b/admins_file.txt dir_b/admins_file_renamed.txt'

echo "
Tentative d'accéder à dir_a/admins_file : "
sudo -u lambda_b bash -c 'cat dir_a/admins_file.txt' 

#L'accès à dir_a est non autorisé pour le group_b, donc la suppression, le rennomage, la lecture et l'écriture aussi.


echo "
Tentative de suppression de dir_c/admins_file : "
sudo -u lambda_b bash -c 'rm -f dir_c/admins_file.txt'

echo "Tentative de rennomage de  dir_c/admins_file : "
sudo -u lambda_b bash -c 'mv dir_c/admins_file.txt dir_c/admins_file_renamed.txt'

echo "Tentative de création d'un fichier dans dir_c : "
sudo -u lambda_b bash -c 'touch dir_c/file.txt' 

echo "
Lecture d'un fichier dans dir_c"
sudo -u lambda_b bash -c 'cat dir_c/admins_file.txt' 



