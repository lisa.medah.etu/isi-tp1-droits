#!/bin/bash

# Création de groupes, utilisateurs et répertoires
#Tout d'abord, nous créons un groupe d'administrateurs pour un éventuel futur administrateur à ajouter

sudo addgroup admins
sudo addgroup groupe_a
sudo addgroup groupe_b


#--disabled-password pour aucune authentification par mot de passe.
#--gecos "" pour que adduser ne demande pas d'information de type finger.

#add in group ...
sudo adduser --disabled-password --gecos "" --ingroup admins admin
sudo adduser --disabled-password --gecos "" --ingroup groupe_a lambda_a
sudo adduser --disabled-password --gecos "" --ingroup groupe_b lambda_b


#Changing the owners and groups owner
sudo mkdir dir_a dir_b dir_c 

sudo chown admin:admins dir_c
sudo chown admin:groupe_a dir_a
sudo chown admin:groupe_b dir_b


# 7 <==> 111 in binary ==> rwx
sudo chmod 770 dir_a dir_b 
sudo chmod g+s dir_a dir_b 

sudo chmod 775 dir_c
sudo chmod +t dir_a dir_b dir_c
