#!/bin/bash

echo "L'accès aux dossiers dans dir_a, dir_b et dir_c :
"
echo "La lecture d'un fichier dans dir_a:"
sudo -u lambda_a bash -c 'cat dir_a/admins_file.txt' 

echo "Modifier un fichier dans dir_a:"
sudo -u lambda_a bash -c 'echo "lambda_a is writing" >> dir_a/admins_file.txt'

echo "Creation d'un fichier dans dir_a:"
sudo -u lambda_a bash -c 'touch dir_a/lambda_a_file.txt && echo "Le fichier a été créé avec succé dans  dir_a avec lambda_a"'

echo "Renommer le fichier qui a été créé par lambda_a dans dir_a:
Nom actuel "
sudo -u lambda_a bash -c ' ls dir_a/ | grep "lambda_a"'
sudo -u lambda_a bash -c 'mv dir_a/lambda_a_file.txt dir_a/lambda_a_file_renamed.txt'
echo "Noveau nom/:"
sudo -u lambda_a bash -c ' ls dir_a/ | grep "lambda_a"'

echo "Tentative de suppression d'un fichier d'admin par   lambda_a dans dir_a:"
sudo -u lambda_a bash -c 'rm -f dir_a/admins_file.txt'

echo "Tentative de rennomage d'un fichier d'admin par lambda_a dans dir_a:"
sudo -u lambda_a bash -c 'mv dir_a/admins_file.txt dir_a/admins_file_renamed.txt'

echo "
Tentative d'accés à dir_b/admins_file : "
sudo -u lambda_a bash -c 'cat dir_b/admins_file.txt' 

#L'accès à dir_b est refusé pour le groupe_a, donc la suppression, le renommage, la lecture et l'écriture aussi.


echo "
tentative de suppression  dir_c/admins_file : "
sudo -u lambda_a bash -c 'rm -f dir_c/admins_file.txt' 

echo "Tentative de rennomage dir_c/admins_file : "
sudo -u lambda_a bash -c 'mv dir_c/admins_file.txt dir_c/admins_file_renamed.txt'

echo "Tentative de création d'un fichier dans dir_c : "
sudo -u lambda_a bash -c 'touch dir_c/file.txt' 

echo "
Lecture d'un fichier dans dir_c"
sudo -u lambda_a bash -c 'cat dir_c/admins_file.txt' 



