#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main(int argc, char *argv[])
{

  FILE *file;
  char c[1000];


  printf(" RUID : %d\n", getuid());
  printf(" RGID : %d\n" , getgid());
  printf(" EUID : %d\n" , geteuid());
  printf(" EGUID : %d\n" , getegid());
  
  file = fopen("../mydir/data.txt" , "r");
  if (file == NULL){

	printf("Access denied\n");
	exit(EXIT_FAILURE);

  }

  c = fgetc(file);
  
  while (c != EOF){
	printf("%c", c);
        c = fgetc(file);
  }

  fclose(file);
  return 0;
  

}
