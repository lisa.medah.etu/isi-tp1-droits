#!/bin/bash

## lamba_a tests

echo -e "> lambda_a essaie de supprimer un fichier dans le répertoire dir_b" \
&& sudo -u lambda_a bash -c './rmg dir_b/lambda_b_file.txt';

echo -e "\n> lambda_a essaie de supprimer un fichier dans le répertoire dir_a en utulisant un mot de passe incorrect" \
&& sudo -u lambda_a bash -c 'echo "incorrect" | ./rmg dir_a/lambda_a_file.txt';

echo -e "\n> lambda_a essaie de supprimer un fichier dans le répertoire dir_a en utulisant un mot de passe correct" \
&& sudo -u lambda_a bash -c 'echo "lambda_a" | ./rmg dir_a/lambda_a_file.txt';

## lambda_b tests

echo -e "\n\n> lambda_b essaie de supprimer un fichier dans le répertoire dir_a" \
&& sudo -u lambda_b bash -c 'echo "lambda_b" | ./rmg dir_a/lambda_a_file.txt';

echo -e "\n> lambda_b essaie de supprimer un fichier dans le répertoire dir_a en utulisant un mot de passe incorrect" \
&& sudo -u lambda_b bash -c 'echo "incorrect" | ./rmg dir_b/lambda_b_file.txt';

echo -e "\n> llambda_a essaie de supprimer un fichier dans le répertoire dir_a en utulisant un mot de passe correct" \
&& sudo -u lambda_b bash -c 'echo "lambda_b" | ./rmg dir_b/lambda_b_file.txt';
