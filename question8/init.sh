#!/bin/bash

# Création de groupes, utilisateurs et répertoires
#Tout d'abord, nous créons un groupe d'administrateurs pour un éventuel futur administrateur à ajouter

sudo addgroup admins
sudo addgroup groupe_a
sudo addgroup groupe_b


#--disabled-password pour aucune authentification par mot de passe.
#--gecos "" pour que adduser ne demande pas d'information de type finger.

#add in group ...
sudo adduser --disabled-password --gecos "" --ingroup admins admin
sudo adduser --disabled-password --gecos "" --ingroup groupe_a lambda_a
sudo adduser --disabled-password --gecos "" --ingroup groupe_b lambda_b

#Changement de propriétaires et de groupes propriétaires
sudo mkdir dir_a dir_b dir_c 

sudo chown admin:admins dir_c
sudo chown admin:groupe_a dir_a
sudo chown admin:groupe_b dir_b


# 7 <==> 111 in binary ==> rwx
sudo chmod 770 dir_a dir_b 
sudo chmod g+s dir_a dir_b 

sudo chmod 775 dir_c
sudo chmod +t dir_a dir_b dir_c

sudo -u lambda_a bash -c 'touch dir_a/lambda_a_file.txt';
sudo -u lambda_b bash -c 'touch dir_b/lambda_b_file.txt';

make && sudo chown admin: rmg && sudo chmod +x,u+s rmg;

sudo -u admin bash -c 'touch /home/admin/passwd'


#1002 est le  uid de lambda_a
sudo -u admin bash -c 'echo  "1002:$(perl -e "print crypt('lambda_a','sa');")" >> /home/admin/passwd'

sudo -u admin bash -c 'echo  "1003:$(perl -e "print crypt('lambda_b','sa');")" >> /home/admin/passwd'
