#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "check_pass.h"

int rmg(const char *filename){

    char psswrd[20];
    char *psswrd_crypte;

    uid_t uid;

    int non_supprime;
    int a;
    uid = getuid();

    printf("ENTEZ UN MOT DE PASSE :\n");

    scanf("%s",psswrd);


    psswrd_crypte = crypt(psswrd, "sa");

    a=check_pass("/home/admin/passwd",psswrd,uid);

    if (check_pass("/home/admin/passwd",psswrd,uid)){
      non_supprime = remove(filename);
      if (!non_supprime){
        printf("%s Supprimé avec succès\n", filename);
      }else{
        printf("%s Suppression impossible\n", filename);
      }
    }else{
      printf("Mot de passe incorrect\n");
    }


    return 0;
}


int main(int argc, char const *argv[]){
    
    struct stat s;
    gid_t gid;


    if(argc != 2)
    {
       printf("Vous devez spécifier le fichier à supprimer\n");
    }


    stat(argv[1], &s);
    gid = getgid();

    if (gid == s.st_gid)
    {
        rmg(argv[1]);
    }
    else
    {

        printf("Vous n'y avez pas accès.\n");

        return EXIT_FAILURE;

    }



    return EXIT_SUCCESS;
}
