#if !defined(CHECK_PASS)
#define CHECK_PASS

int check_pass(char *filename,  char *password, uid_t user_id);

#endif
