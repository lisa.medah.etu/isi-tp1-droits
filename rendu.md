# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- MEDAH, Lisa, email: lisa.medah.etu@univ-lille.fr

- Nom, Prénom, email: ___

## Question 1
Oui, le processus peut écrire sur le fichier.
Pourquoi? On a l’utilisateur **toto** appartient au group **ubuntu** et d'après les droits d'accés: -r--r**w**-r--, tous les utilisateurs du groupe ubuntu ont droit d'ouverture en mode ériture.

## Question 2

- Le caractere x pour un répetoire signifie que ce dernier est exécutable et qu'on peut accéder à l'ensemble de ses sous-répertoires.

- Après suppression du droit d'exécution au groupe **ubuntu**, on ne peut pas entrer dans le répertoire **mydir** avec l'utilisateur **toto** ( **Permission denied** ) car ce dernier appartient au groupe **ubuntu** et celui-ci à plus le droit **x** ( exécution), ce qui signifie que tout utilisateur appartenant au groupe ubuntu n'a pas accés à ce répetoire et ses details.

- Avec l'utilisateur **toto**, on ne peut pas lister le contenu du répertoire **mydir** car **toto** appartient au groupe ubuntu et ce dernier n'a plus le droit d'exécution donc l'accés au contenu du répertoire n'est pas autorisé. 

## Question 3



## Question 4

Réponse

## Question 5

- La commande **chfn** permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd"
- **-rwsr-xr-x** : - Tout le monde a le droit de lecture et d'exécution.
                   - Le droit d'écriture que pour le proprietaire root.
                   - Le flag **s** est activé.
 
## Question 6

- Les mots de passes des utilisateurs sont stockés dans **/etc/shadow** où les mots de passes sont cryptés.

- Ils sont stockés dans **/etc/shadow** pour augmenter la securité de son contenu, car seul **root** à les droits pour y acceder (ils ne sont pas stockés dans **passwd** car il est lisible par tout le monde et cela represente un risque).


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








